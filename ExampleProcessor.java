package src.main.java;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import com.google.api.services.gmail.model.ListDraftsResponse;

public class ExampleProcessor implements Processor {

	@Override
	public void process(Exchange e) throws Exception {
		Message inMessage = e.getIn();
		
		ListDraftsResponse resp = inMessage.getBody(ListDraftsResponse.class);
		
		System.out.println(resp);
		
		e.getIn().setBody(resp.getDrafts());
		
	}

}
