Simple Gmail archiver

This app takes unread mails from your Gmail account, filters then if they are not from noreply address and saves then to you Google Drive

For this to work you need to login... the login page will popup when you start the app
Create a label in your Gmail and write id of that label to properties.properties
And also add a folderId you'd like to save your emails to (root for just saving into your Google Drive no extra folder)


Camel Project for Spring 
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

For more help see the Apache Camel documentation

    http://camel.apache.org/
