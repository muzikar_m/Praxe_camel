package com.mycompany.java;

import java.awt.Desktop;
import java.net.URL;
import java.util.Collections;

import com.google.appengine.repackaged.com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.appengine.repackaged.com.google.api.client.auth.oauth2.AuthorizationRequestUrl;
import com.google.appengine.repackaged.com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.appengine.repackaged.com.google.api.client.http.javanet.NetHttpTransport;
import com.google.appengine.repackaged.com.google.api.client.json.jackson2.JacksonFactory;

public class Helper {
	private void openWebpage(String urlString) {
		try {
			if (Desktop.isDesktopSupported()){
				System.out.println("Opening login URL in the default browser");
				Desktop.getDesktop().browse(new URL(urlString).toURI());
				System.out.println("Pleaser refresh the page if you've already given permissions to this app");
			}
			else {
				System.out.println("Can't open browser login with this url: " +urlString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void init() {
		System.out.println("INIT");
        AuthorizationCodeFlow f = new GoogleAuthorizationCodeFlow
                .Builder(new NetHttpTransport(),
                JacksonFactory.getDefaultInstance(),
                Constants.CLIENT_ID,
                Constants.CLIENT_SECRET,
                // request access to following scopes:
                Collections.singletonList("https://www.googleapis.com/auth/drive https://mail.google.com"))
                .build();

        AuthorizationRequestUrl url = f.newAuthorizationUrl()
                .setRedirectUri("http://127.0.0.1:8443/handler/");
		openWebpage(url.toString());
	}

}
