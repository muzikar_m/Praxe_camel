package com.mycompany.java;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

public class GauthRoute extends RouteBuilder {

	/*String clientId = "535824807985-mca1el1svetrn2ermpitj1e00dc114kk.apps.googleusercontent.com";
	String clientSecret = "Wsse8j2yxiI2nZ7zhGTNsk2w";*/

	@Override
	public void configure() throws Exception {

		from("jetty:http://127.0.0.1:8443/handler/?useContinuation=false").log("code =${body}")
				.setHeader(Exchange.HTTP_METHOD, constant("POST"))
				.setHeader(Exchange.HTTP_QUERY,
						simple("code=${headers.code}&client_id=" + Constants.CLIENT_ID + "&client_secret=" + Constants.CLIENT_SECRET
								+ "&redirect_uri=http://127.0.0.1:8443/handler/&grant_type=authorization_code"))
				.to("https://www.googleapis.com/oauth2/v3/token?bridgeEndpoint=true")
				.unmarshal().gzip()
				.log("${body}").process(new TokenProcessor())
				.removeHeader("Content-Encoding")
				.setBody(constant("Jste přihlášen"));
				
		from("timer://init?repeatCount=1")
			.bean(Helper.class, "init");
	}

}