package com.mycompany.java;

//import com.google.appengine.repackaged.com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

import com.google.appengine.repackaged.com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.appengine.repackaged.com.google.api.client.auth.oauth2.AuthorizationRequestUrl;
import com.google.appengine.repackaged.com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.appengine.repackaged.com.google.api.client.http.javanet.NetHttpTransport;
import com.google.appengine.repackaged.com.google.api.client.json.jackson2.JacksonFactory;

import java.util.Collections;

public class AuthTest {
    public static void main(String[] args) {

        String projectKey = "AIzaSyDpKxZWkmDc_oHZgEN-poRkVUPugN4rm80";

        String clientId = "255224565390-0rp4focc7imp9f2knlf6b2309lu0is8i.apps.googleusercontent.com";
        String clientSecret = "X4JBRlZw9s9ctdOw9xg78sf2";

        AuthorizationCodeFlow f = new GoogleAuthorizationCodeFlow
                .Builder(new NetHttpTransport(),
                JacksonFactory.getDefaultInstance(),
                clientId,
                clientSecret,
                // request access to following scopes:
                Collections.singletonList("https://www.googleapis.com/auth/drive"))
                .build();

        AuthorizationRequestUrl url = f.newAuthorizationUrl()
                .setRedirectUri("http://localhost:8080");
//                .setScopes(Collections.singletonList("https://www.googleapis.com/auth/drive"));
        System.out.println(url);
    }
}
