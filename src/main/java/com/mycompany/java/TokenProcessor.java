package com.mycompany.java;

import com.mycompany.java.Constants;

import java.io.InputStream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.google.drive.GoogleDriveComponent;
import org.apache.camel.component.google.drive.GoogleDriveConfiguration;
import org.apache.camel.component.google.mail.GoogleMailComponent;
import org.apache.camel.component.google.mail.GoogleMailConfiguration;
import org.apache.camel.model.RoutesDefinition;

import com.google.appengine.repackaged.com.google.gson.JsonObject;
import com.google.appengine.repackaged.com.google.gson.JsonParser;

public class TokenProcessor implements Processor{
	
	@Override
	public void process(Exchange e) throws Exception {
		JsonObject json = new JsonParser().parse(e.getIn().getBody(String.class)).getAsJsonObject();
		e.getIn().setBody(json);
		String accessToken = json.get("access_token").getAsString();
		System.out.println("NEW ACCESS TOKEN: " + accessToken);
		if (e.getContext().getComponent("g-mail") != null)
			return;
		GoogleMailComponent gmail = new GoogleMailComponent(e.getContext());
		GoogleMailConfiguration conf = new GoogleMailConfiguration();
		conf.setAccessToken(accessToken);
		conf.setClientId(Constants.CLIENT_ID);
		conf.setClientSecret(Constants.CLIENT_SECRET);
		conf.setApplicationName("Gmail test");
		gmail.setConfiguration(conf);
		e.getContext().addComponent("g-mail", gmail);
		GoogleDriveComponent gdrive = new GoogleDriveComponent(e.getContext());
		GoogleDriveConfiguration drconf = new GoogleDriveConfiguration();
		drconf.setAccessToken(accessToken);
		drconf.setClientId(Constants.CLIENT_ID);
		drconf.setClientSecret(Constants.CLIENT_SECRET);
		drconf.setApplicationName("Gdrive test");
		gdrive.setConfiguration(drconf);
		e.getContext().addComponent("g-drive", gdrive);
		System.out.println("Creating routes");
		try(InputStream is = getClass().getResourceAsStream("/routes.xml")){
			RoutesDefinition routes = e.getContext().loadRoutesDefinition(is);
			e.getContext().addRouteDefinitions(routes.getRoutes());
		}
		//InputStream is = getClass().getResourceAsStream("/routes.xml");
		//InputStream is = new FileInputStream("src/main/resources/META-INF/spring/routes.xml");
		System.out.println("All routes are now running");
		
	}
}

