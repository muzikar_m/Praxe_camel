package com.mycompany.java;


import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.google.api.client.json.GenericJson;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;


public class ExampleProcessor implements Processor {

	@Override
	public void process(Exchange e) throws Exception {
		GenericJson json = e.getIn().getBody(GenericJson.class);
		Message resp;
		if (!(json instanceof Message))
			resp = (Message)json.get("message");
		else
			resp = (Message)json;
		for (MessagePartHeader mh : resp.getPayload().getHeaders())
			e.getIn().setHeader(mh.getName(), mh.getValue());
	}
}
