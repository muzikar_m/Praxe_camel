package com.mycompany.java;

import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.google.mail.internal.GoogleMailConstants;

import com.google.api.services.gmail.model.ModifyMessageRequest;

public class Modifier implements Processor{
	
	@Override
	public void process(Exchange e) throws Exception {
		String labelId = e.getIn().getHeader("mycompany.mail.labelid", String.class);
		List<String> l = Arrays.asList(labelId);
		e.getIn().setHeader(GoogleMailConstants.PROPERTY_PREFIX  + "modifyMessageRequest", new ModifyMessageRequest().setAddLabelIds(l));
	}

}
