package com.mycompany.java;

import java.io.PrintWriter;
import java.util.Arrays;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.google.drive.internal.GoogleDriveConstants;
import org.apache.commons.codec.binary.Base64;

import com.google.api.client.http.FileContent;
import com.google.api.client.util.StringUtils;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;

public class MailProcessor implements Processor {

	@Override
	public void process(Exchange e) throws Exception {
		Message m = (Message) e.getIn().getBody();

		String folderId = e.getIn().getHeader(Constants.DRIVE_FOLDER_ID, String.class);

		File f = new File();
		f.setParents(Arrays.asList(new ParentReference().setId(folderId)));
		f.setTitle((String) e.getIn().getHeader("From") + ": " + e.getIn().getHeader("Subject") + " "
				+ e.getIn().getHeader("Received"));
		try (PrintWriter writer = new PrintWriter("temp-mail.txt", "UTF-8")) {
			writer.println("FROM: " + e.getIn().getHeader("From"));
			writer.println("TO: " + e.getIn().getHeader("To"));
			writer.println("SUBJECT" + e.getIn().getHeader("Subject"));
			writer.println();
			writer.println("MESSAGE: ");
			if (m.getPayload().getBody().getData() != null) {
				writer.println(
						StringUtils.newStringUtf8(Base64.decodeBase64(m.getPayload().getBody().getData().getBytes())));
			} else {
				for (MessagePart mp : m.getPayload().getParts()) {
					if (mp.getBody().getData() != null && mp.getMimeType() != "text/html") {
						String data = mp.getBody().getData().replace("_", "/").replace("-", "+");
						writer.println(StringUtils.newStringUtf8(Base64.decodeBase64(data)));
					}
				}
			}
		}
		java.io.File file = new java.io.File("temp-mail.txt");
		FileContent mediaContent = new FileContent("text/plain", file);
		e.getIn().setHeader(GoogleDriveConstants.PROPERTY_PREFIX + "mediaContent", mediaContent);
		e.getIn().setHeader(GoogleDriveConstants.PROPERTY_PREFIX + "content", f);
	}

}
